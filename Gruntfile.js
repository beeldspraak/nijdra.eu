module.exports = function(grunt) {
	
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		less: {
			development: {
				options: {
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: { 
					'assets/dist/css/nijdra.eu.min.css':'assets/src/less/nijdra/nijdra.less',
					'assets/dist/css/nijdra.eu.wygwam.min.css':'assets/src/less/nijdra/wygwam.less'
				}
			}
		},
		concat: {
			bootstrap_js: {
				src: [
					'assets/src/js/libs/bootstrap/transition.js',
					'assets/src/js/libs/bootstrap/alert.js',
					'assets/src/js/libs/bootstrap/button.js',
					'assets/src/js/libs/bootstrap/carousel.js',
					'assets/src/js/libs/bootstrap/collapse.js',
					'assets/src/js/libs/bootstrap/dropdown.js',
					'assets/src/js/libs/bootstrap/modal.js',
					'assets/src/js/libs/bootstrap/tooltip.js',
					'assets/src/js/libs/bootstrap/popover.js',
					'assets/src/js/libs/bootstrap/scrollspy.js',
					'assets/src/js/libs/bootstrap/tab.js',
					'assets/src/js/libs/bootstrap/affix.js'
				],
				dest: 'assets/src/js/libs/bootstrap/bootstrap.js'
			}
		},
		copy: {
			fonts: {
				expand: true,
				cwd: 'assets/src',
				src: 'fonts/**',
				dest: 'assets/dist/'
			},
			jquery: {
				expand: true,
				cwd: 'assets/src',
				src: 'js/jquery/**',
				dest: 'assets/dist/'
			},
			css: {
				expand: true,
				cwd: 'assets/src',
				src: 'css/**',
				dest: 'assets/dist/'
			},
			img: {
				expand: true,
				cwd: 'assets/src',
				src: 'img/**',
				dest: 'assets/dist/'
			},
			ico: {
				expand: true,
				cwd: 'assets/src',
				src: 'ico/**',
				dest: 'assets/dist/'
			},
			js: {
				expand: true,
				cwd: 'assets/src',
				src: 'js/**',
				dest: 'assets/dist/'
			}
		},
		watch: {
            src: {
                files: ['*.html'],
                options: { livereload: true }
            },
			styles: {
				files: ['assets/src/less/**/*.less'], // which files to watch
				tasks: ['less'],
				options: {
					spawn: false,
					livereload: true
				}
			}
		},
		uglify: {
			build_en: {
				files: {
					'assets/dist/js/nijdra.eu.min_en.js': [
						'<%= concat.bootstrap_js.dest %>',
						'assets/src/js/libs/fancybox/jquery.fancybox.js',
						'assets/src/js/libs/smooth-scroll/jquery.smooth-scroll.js',
						'assets/src/js/libs/validate/jquery.validate.js',
						'assets/src/js/libs/validate/additional-methods.js',
						'assets/src/js/libs/validate/localization/messages_en.js',
						'assets/src/js/libs/slick/slick.min.js',
						'assets/src/js/libs/wow/wow.min.js',
						'assets/src/js/init.js'
					]
				}
			},
			build_nl: {
				files: {
					'assets/dist/js/nijdra.eu.min_nl.js': [
						'<%= concat.bootstrap_js.dest %>',
						'assets/src/js/libs/fancybox/jquery.fancybox.js',
						'assets/src/js/libs/smooth-scroll/jquery.smooth-scroll.js',
						'assets/src/js/libs/validate/jquery.validate.js',
						'assets/src/js/libs/validate/additional-methods.js',
						'assets/src/js/libs/validate/localization/messages_nl.js',
						'assets/src/js/libs/slick/slick.min.js',
						'assets/src/js/libs/wow/wow.min.js',
						'assets/src/js/init.js'
					]
				}
			},
			build_de: {
				files: {
					'assets/dist/js/nijdra.eu.min_de.js': [
						'<%= concat.bootstrap_js.dest %>',
						'assets/src/js/libs/fancybox/jquery.fancybox.js',
						'assets/src/js/libs/smooth-scroll/jquery.smooth-scroll.js',
						'assets/src/js/libs/validate/jquery.validate.js',
						'assets/src/js/libs/validate/additional-methods.js',
						'assets/src/js/libs/validate/localization/messages_de.js',
						'assets/src/js/libs/slick/slick.min.js',
						'assets/src/js/libs/wow/wow.min.js',
						'assets/src/js/init.js'
					]
				}
			}
		}
	});
			
	grunt.registerTask('default', ['less', 'concat', 'copy', 'watch', 'uglify']);
};