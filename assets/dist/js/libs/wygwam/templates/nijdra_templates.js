CKEDITOR.addTemplates( 'default',
{
    // Template definitions.
    templates :[
        {
            title: 'Tweekoloms lay-out',
            html:
                '<div class="row">' +
	                '<div class="col-md-6">Kolom 1</div>' +
	                '<div class="col-md-6">Kolom 2</div>' +
	            '</div>'
        },
        {
            title: 'Driekoloms lay-out',
            html:
                '<div class="row">' +
	                '<div class="col-md-4">Kolom 1</div>' +
	                '<div class="col-md-4">Kolom 2</div>' +
	                '<div class="col-md-4">Kolom 3</div>' +
	            '</div>'
        },
    ]
});