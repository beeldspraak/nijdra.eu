$(document).ready(function(){
	
	$('#nijdra_eu.view-index #carousel').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		infinite: true,
		speed: 1000,
		fade: true,
		cssEase: 'linear',
		dots: false,
		arrows: true,
		appendArrows: $('#carousel #carousel-controls'),
		prevArrow: $('#carousel-controls .prev'),
		nextArrow: $('#carousel-controls .next')
	});
	
    $(window).load(function() {
      $('#nijdra_eu.view-index #carousel .slide').height($('#carousel').height());
    });
  
    $(window).resize(function() {
      $('#nijdra_eu.view-index #carousel .slide').height($('#carousel').height());
    });	
	
	$('.sectors .carousel ul').slick({
		autoplay: false,
		autoplaySpeed: 5000,
		slidesToScroll: 1,
		slidesToShow: 3,
		infinite: true,
		cssEase: 'linear',
		dots: true,
		appendDots: $('.sectors #sector-pagination'),
		arrows: true,
		centerMode: true,
		centerPadding: 0,
		appendArrows: $('.sectors #sector-controls'),
		prevArrow: $('.sectors #sector-controls .prev'),
		nextArrow: $('.sectors #sector-controls .next'),
		responsive: [{
			breakpoint: 992,
			settings: {
				arrows: false,
				centerMode: true,
				slidesToShow: 3
			}
		},
		{
			breakpoint: 768,
			settings: {
				arrows: false,
				centerMode: true,
				slidesToShow: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				arrows: false,
				centerMode: true,
				slidesToShow: 1
			}
		}]
	});

    $('.fancybox').fancybox({
	    autoWidth: false,
	    autoHeight: false,
    	width: "90%",
    	heigth: "90%",
    	minWidth: 800,
    	minHeight: 600,
    	maxWidth: 1440
		
    });
	
	$('#navbar .nav > li.has-children').hover(function() {
		$(this).toggleClass('open');
	});
	
	$('.form-validate').validate();
	
    $('a[href*=\\#]').smoothScroll({
	    offset: - ($('[data-scroll-header]').outerHeight())
	});
	
	new WOW().init();
    
    $('.page-header .navbar-top .navbar-form .form-control').focusin(function(){
	    $(this).attr('placeholder',  $(this).data('placeholder'));
    }).focusout(function(){
	    $(this).attr('placeholder', ' ');
    })
    
});