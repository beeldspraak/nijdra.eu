<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Default
 *
 * @package             Default
 * @author              Rein de Vries (info@reinos.nl)
 * @copyright           Copyright (c) 2016 Rein de Vries
 * @license  			http://reinos.nl/add-ons/commercial-license
 * @link                http://reinos.nl/add-ons/gmaps
 */

require_once(PATH_THIRD.'freebie/config.php');

return array(
    'author'      => FREEBIE_AUTHOR,
    'author_url'  => FREEBIE_AUTHOR_URL,
    'name'        => FREEBIE_NAME,
    'description' => FREEBIE_DESCRIPTION,
    'version'     => FREEBIE_VERSION,
    'docs_url'  => FREEBIE_DOCS,
    'settings_exist' => TRUE,
    'namespace'   => 'Reinos\Freebie',
);