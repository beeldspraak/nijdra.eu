<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Default config
 *
 * @package		Default module
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link		http://reinos.nl
 * @copyright 	Copyright (c) 2016 Reinos.nl Internet Media
 */

//contants
if ( ! defined('FREEBIE_NAME'))
{
	define('FREEBIE_NAME', 'Freebie');
	define('FREEBIE_CLASS', 'Freebie');
	define('FREEBIE_MAP', 'freebie');
	define('FREEBIE_VERSION', '2.0.4');
	define('FREEBIE_DESCRIPTION', 'Tell EE to ignore specific segments when routing URLs');
	define('FREEBIE_DOCS', 'http://docs.reinos.nl/freebie');
	define('FREEBIE_AUTHOR', 'Rein de Vries');
	define('FREEBIE_AUTHOR_URL', 'http://reinos.nl/add-ons');
	define('FREEBIE_STATS_URL', 'http://reinos.nl/index.php/module_stats_api/v1');
}

//configs
$config['name'] = FREEBIE_NAME;
$config['version'] = FREEBIE_VERSION;

//load compat file
require_once(PATH_THIRD.FREEBIE_MAP.'/compat.php');

/* End of file config.php */
/* Location: /system/expressionengine/third_party/default/config.php */