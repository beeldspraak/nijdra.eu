<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Default
 *
 * @package             Google Sitemap Lite
 * @author              Rein de Vries (info@reinos.nl)
 * @copyright           Copyright (c) 2016 Rein de Vries
 * @license  			http://reinos.nl/add-ons/commercial-license
 * @link                http://reinos.nl/add-ons/gmaps
 */

require_once(PATH_THIRD.'google_sitemap_lite/config.php');

return array(
    'author'            => GSL_AUTHOR,
    'author_url'        => GSL_AUTHOR_URL,
    'name'              => GSL_NAME,
    'description'       => GSL_DESCRIPTION,
    'version'           => GSL_VERSION,
    'docs_url'          => GSL_DOCS,
    'settings_exist'    => false,
    'namespace'         => 'Reinos\GoogleSitemapLite'
);
