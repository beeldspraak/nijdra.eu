<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Compatibility file
 *
 * @package		Default
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @license  	http://reinos.nl/add-ons/commercial-license
 * @link        http://reinos.nl/add-ons/gmaps
 * @copyright 	Copyright (c) 2016 Reinos.nl Internet Media
 */


//--------------------------------------------
//	short dump version
//--------------------------------------------
if ( ! function_exists('dumper'))
{
	function dumper($data = '', $stop = false)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';

		if($stop)
		{
			exit;
		}
	}
}