<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Google Sitemap Lite lib file
 *
 * @package		Google Sitemap Lite
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link        http://reinos.nl/add-ons/google-sitemap-lite
 * @copyright 	Copyright (c) 2013 Reinos.nl Internet Media
 */

class Google_sitemap_lite_lib
{

    /**
     * Load the right File to format the api
     *
     * @param $type
     * @param string $alias
     */
    public function load($type, $alias = 'google_sitemap_lite_api')
    {
        switch($type)
        {
            case 'structure':

                if(ee('Addon')->get('structure') !== NULL && ee('Addon')->get('structure')->isInstalled())
                {
                    ee()->load->library('google_sitemap_lite_structure', null, $alias);
                    return;
                }

                show_error($type.' not installed, but called for the Google Sitemap Lite module');

                break;
        }

        show_error($type.' is not supported for the Google Sitemap Lite module.');
    }

    //--------------------------------------------------------------

    /**
     * Function added to find and replace required escape characters in line with sitemaps.org/protocol.html
     *
     * @param type $val
     * @return type
     */
    public function loc_escapes($val)
    {
        $val = str_replace('&', "&amp;", $val);
        $val = str_replace('', "&apos;", $val);
        $val = str_replace('>',"&gt;", $val);
        $val = str_replace('<', "&lt;", $val);
        $val = str_replace('"', "&quot;", $val);

        return $val;
    }


    //--------------------------------------------------------------

    /**
     * Remove empty alues from array
     *
     * @param $array
     * @return unknown_type
     */
    public function clean_up_array($array)
    {
        foreach ($array as $key => $value) {
            $value = trim($value);
            if (is_null($value) || empty($value)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    //--------------------------------------------------------------

    /**
     * Format the xml
     *
     * @return unknown_type
     */
    public function format_sitemap($sitemap)
    {
        $format = "\n";

        if(!empty($sitemap))
        {
            foreach($sitemap as $key => $val)
            {
                //is there a link
                if(!empty($val['loc']))
                {
                    //site url and index
                    $site_url = ee()->functions->fetch_site_index();

                    //bestaat de url al met http en www
                    if (!preg_match("/www./i", $val['loc']) && !preg_match("/http/i", $val['loc'])) {
                        //add a slash when he is not there
                        $slash = $val['loc'][0] == '/'?'':'/';
                        $val['loc'] = $site_url.$slash.$val['loc'];
                    }

                    // remove double slashes
                    $val['loc'] = preg_replace("#(^|[^:])//+#", "\\1/", $val['loc']);

                    //format the xml
                    $format .= "<url>\n";
                    $format .= "<loc>".$val['loc']."</loc>\n";
                    $format .= "<lastmod>".$val['lastmod']."</lastmod>\n";
                    $format .= "<changefreq>".$val['changefreq']."</changefreq>\n";
                    $format .= "<priority>".$val['priority']."</priority>\n";
                    $format .= "</url>\n";
                }
            }
        }
        return $format;
    }
}
