<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Google Sitemap Lite
 *
 * @package		Google Sitemap Lite
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link        http://reinos.nl/add-ons/google-sitemap-lite
 * @copyright 	Copyright (c) 2013 Reinos.nl Internet Media
 */

require_once(PATH_THIRD.'google_sitemap_lite/config.php');

$plugin_info = array(
	'pi_name'        => GSL_NAME,
	'pi_version'     => GSL_VERSION,
	'pi_author'      => GSL_AUTHOR,
	'pi_author_url'  => GSL_DOCS,
	'pi_description' => GSL_DESCRIPTION
  );


class Google_sitemap_lite
{
	private $structure_version  		= '';
	
	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{

	}
	
	/**
	 * Generate the sitemap
	 * 
	 * @return unknown_type
	 */
	public function generate()
	{
		$options['site_id'] = ee()->TMPL->fetch_param('site_id') == '' ? ee()->config->item('site_id') : ee()->TMPL->fetch_param('site_id');
		$options['changefreq'] = ee()->TMPL->fetch_param('changefreq', 'weekly');
		$options['changefreq_listing'] = ee()->TMPL->fetch_param('changefreq_listing', 'daily');
		$options['prio'] = ee()->TMPL->fetch_param('prio', '0.8');
		$options['prio_listing'] = ee()->TMPL->fetch_param('prio_listing', '0.5');
		$options['prio_homepage'] = ee()->TMPL->fetch_param('prio_homepage', '1');
		$options['exclude'] = ee()->TMPL->fetch_param('exclude');

		//fetch the mode
		$type = ee()->TMPL->fetch_param('type') != ''? strtolower(ee()->TMPL->fetch_param('type')) : '';

		//load the lib
		ee()->load->library('google_sitemap_lite_lib');

		//load the correct sitemap and assign it to 'google_sitemap_lite_api'
		ee()->google_sitemap_lite_lib->load($type, 'google_sitemap_lite_api');

		//set the options
		ee()->google_sitemap_lite_api->set_options($options);

		//set the site url
		ee()->google_sitemap_lite_api->set_site_url();

		//add the root item
		$sitemap[] = array(
			'loc' => ee()->google_sitemap_lite_api->site_url,
			'lastmod' => date('Y-m-d'),
			'changefreq' => ee()->google_sitemap_lite_api->options['changefreq'],
			'priority' => ee()->google_sitemap_lite_api->options['prio_homepage']
		);

		$sitemap = array_merge($sitemap, ee()->google_sitemap_lite_api->build_sitemap($type));

		//format the xml and print it to the screen
		$format = '<?xml version="1.0" encoding="utf-8"?>';
		$format .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		if(!empty($sitemap))
		{
			$format .= ee()->google_sitemap_lite_lib->format_sitemap($sitemap);
		}
		$format .= '</urlset>';

		return $format;
		
	}
}
