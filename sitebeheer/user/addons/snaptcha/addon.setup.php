<?php

return array(
	'author'      => 'PutYourLightsOn',
	'author_url'  => 'https://www.putyourlightson.net/',
	'name'        => 'Snaptcha',
	'version'     => '2.0.3',
	'namespace'   => '\\',
	'settings_exist' => TRUE,
 	'docs_url'    => 'https://www.putyourlightson.net/snaptcha/docs',
);
