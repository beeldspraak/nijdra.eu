<?php

	return array(
		'name'               => 'Structure',
		'version'            => '4.1.16',
		'description'        => 'Create pages, generate navigation, manage content through a simple interface and build robust sites faster than ever.',
		'namespace'          => 'Structure',
		'author'             => 'EEHarbor',
		'author_url'         => 'https://eeharbor.com/structure',
		'docs_url'           => 'http://buildwithstructure.com',
		'settings_exist'     => true,
		'fieldtypes' => array(
		  'structure' => array(
		    'name' => 'Structure'
		  )
		)
	);